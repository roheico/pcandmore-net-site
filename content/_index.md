---
title: "Welcome"
---

Welcome to my blog. Here you can find content on software, linux and real life topics. Enjoy your stay! 

You can follow this site via the RSS feed if you wish, and it does serve full-length versions of my posts. If you want to know more or would like to send me a message, you can take a look at the [about](/about) and [contact](/contact) pages.

[Codeberg profile](https://codeberg.org/roheico)