---
title: "Contact"
date: 2023-08-06
---

If you want to get in touch with me, you can contact me through the following channels.

Email: roman@pcandmore.net  
XMPP: roheico@pcandmore.net