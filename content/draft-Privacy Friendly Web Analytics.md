---
title: "Privacy Friendly Web Analytics"
date: 2024-03-01
---

Recently, I implemented privacy-friendly analytics for this website. "Privacy" and "Analytics" seem to exclude eachother, but they don't have to. And while one might be tempted to use something like Google Analytics, that would be against the character of my blog. So here is what I did instead.

### The Goal

My site currently includes no direct means of monetization. There is no data I sell, and there are no ads plastered over the site. I do not want to collect data in any intrusive way. The kind of analytics that is relevant here are things like a visit counter and stats on the ratio of bots and humans. Also, there is the goal of keeping the site's JavaScript and cookie footprint at or around 0.

### Who To Monitor?

When running a web server, **there are two parties I can monitor. One is the user, the other one is my web server**. The user can be tracked by a snippet of JavaScript code. That is not necessarily problematic, as this snippet can be **1)** *limited in its tracking* and **2)** *collect data for a trustworthy, self-hosted analytics engine*. But as said before, I would like to keep things minimal and avoid unnecessary JS and cookies.

Therefore, monitoring the other possible party - the web server - is more appealing. This can be done by processing the technical log(s) that the server produces anyways. This has no impact on the inner workings of your web browser, and it is enough for the simple kind of analytics that I am interested in. So, I went with processing my own logs.

### The Implemented Solution

The analytics engine that won this race is matomo. It is a selfhosted, GPL-licensed solution that supports JS tracking and log processing. I wrote a simple job that periodically feeds the log data into matomo, and matomo offers the toolset to display this data in an appealing dashboard. There was some troubleshooting involved with the NixOS package for my log-based setup, but it was managable. I can answer questions if you want such a setup too.

The solution has been in operation for a while now, but the built-in bot filter seems to not be working for my specific setup. Maybe that can be fixed by changing something in the configuration, or by mass-blocking the obvious bots (or blocking IP-ranges of the cloud). 

While the setup is not perfect or mature (still suffering under heavy log pollution), I am happy to have a privacy-friendly way to see the traffic I'm getting in a dashboard. If you are looking for an analytics solution, this might be a good option for you!
