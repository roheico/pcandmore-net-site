---
title: "Getting Back Into Cycling, Buying Barefoot Shoes"
date: 2024-03-16
---

After some inactivity over here, I am doing an IRL update post. Actually, I did publish a post recently. It was covering a webserver log based, privacy friendly analytics setup. But I unpublished the post again and am planning to re-visit privacy friendly analytics in the future, since the setup turned out too dysfunctional to be satisfying.

As you can tell from the title, this post is about some enjoyment i've had outside - which must be a thing that us very tech-involved people often lack.

### Cycling

Recently, I have gotten back into cycling. While winter has been fading away, I recently went down into the basement to get my bicycle out for a ride. I bought it some time in the recent years, but haven't used it as much as I would like to have. And wow, it is a thrill! 

I do not really live in the country, and it would take a while to walk out of town for me. But with my bicyle, I can reach the woods and fields quite quickly for a quick trip - which is much more interesting to me than just slowly moving through the neighbouring streets.

Last weekend, I decided to do a shopping trip on my (non-electric) bicycle rather than using my car. Even though it would have been a faster ride by car, cycling there and back for something like 2 and a half hours in total was awesome. I got to have sunlight on my skin and neither paid nor stressed over a parking spot for my car. It was especially doable since I only wanted to buy a single product, rather than having to carry an entire bag of groceries.

Yesterday I did a cycling trip with my friend. We rode through some rain and over wet ground in the woods. Both of us were wearing white sweaters, which were full of small brown dots afterwards. That was pretty fun.

### Getting Barefoot Shoes

You have probably seen the very obvious and unusual kind of barefoot shoes: the ones with separate toes. While I am married and therefore don't need to think about alienating a potential partner by making questionable fashion choices, I still went with a pair that closely resembles normal shoes. It has been one week of wearing them, and I am happy with the purchase so far.

It makes sence to me that walking barefoot would be good for us. Even though the opinions on barefoot shoes and their impact on our health vary, they indeed do bring us closer to the barefoot experience. 

Appearently you are supposed to take it slow at first and not do too much of a hard transition. I did not mind that too much, and consequently had some light pain in my back at first. But now, after one week, I feel like my body has adjusted and am happy with the experience they provide. Might even buy another, more robust pair for long hiking trips. I did some long distance hiking over the recent years and that is something I would like to get back into!

