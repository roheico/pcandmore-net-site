---
title: "Writing Documents In Neovim"
date: 2023-09-15
---

If you are used to editing config files and simple plaintext notes in Vim/Neovim, it can feel limiting to use a MS-Word-esque editor for your documents. Even though Vim is not your typical PDF-producing document-editor, it is quite easy to turn it into a serious document writing solution. 

### What you need:
* Vim/Neovim
* a processor for the document format of your choice (LaTeX, Markdown, Asciidoc...)
* a way to turn your documents into PDF output
* a PDF reader of your choice

### Choice of tools

Currently I run a setup that allows me to have my editor (Neovim) on one half of the screen and a browser window on the other half. This allows me to do research or to translate a quote easily, while being able to view my document in a floating window by hitting a shortcut in Neovim. Here is what it looks like:

![my setup](/pictures/neovim-adoc-demo.gif)

For the document format, I chose Asciidoc. After using both LaTeX for a large technical document and Markdown for writing web content, Asciidoc feels a lot as if those two had a child. It has a really simple syntax, like Markdown, while providing a more featureful and flexible experience. I use Asciidoctor to process my Asciidoc writing.

For the PDF viewer I use MuPDF. It produces a minimalist PDF view and even allows me to invert the colors. 

### How to do it

1. Install the document format processor of your choice. I use Asciidoctor for processing Asciidoc documents.

_On NixOS, my Asciidoctor package came with a PDF converter. If the Asciidoctor package you installed does not come with a PDF converter, make sure to install the converter separately. If you want to write in another document format, make sure you find out how to produce a PDF from your writing._

2. Install MuPDF. Alternatively, use another PDF viewer.

3. Configure a Neovim keybinding to produce the PDF and open the file with the PDF viewer. You can do this as follows in your lua configuration:

----
```
vim.g.mapleader = " "
vim.keymap.set("n", "<leader>p", ":! asciidoctor-pdf \"%\" -o result.pdf && mupdf result.pdf<CR>")
```
----

This way, pressing space+p in normal mode will produce the PDF and open it, giving you a view of the current result.