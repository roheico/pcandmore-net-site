---
title: "Kicking The Addiction™"
date: 2024-10-26
---

About a month ago, I brought up in a post that I was abstaining from YouTube. I have been sticking with that, and, if any, only watched 2-ish videos on there since then. And I already see that as one of my best life decisions and feel less dependant on artificial stimulation.

It has been surprisingly easy. PeerTube served as a filler for that gap, but at this point my favourite instance is more or less exhausted. One thing I have also done was downloading gigabytes of sermons to listen to offline. Now that my content consumption habit has been severely restricted, I find it much easier to listen to this kind of informative, less-stimulating content. Currenly, I'm going through a series of audio sermons on the Gospel of Matthew that has weeks worth of material. It has been genuinely fascinating!

Making this lifestyle change is something that was very much in line with my personal principles, and it feels great to be abiding by them more authentically now. By **the Addiction™**, as in the post title, I mean this obviously unhealthy media consumption habit (which probably is an actual addiction).

Your Addiction™ might be different from mine, if you happen to be in the same boat. You might be a highly invested Twitter person, for example. Whatever your Addiction™ of this sort is, It will have one thing in common with mine. Namely, the Trade Mark sign (™) - Because it is based on artificial, hamster-wheel-like, (probably) commercial consumption engines.

One of the worst things about desensitizing outselves to healthy levels of mental stimulation is that it makes the useful and real activities in our life boring to us. I encourage you to take the step and wave goodbye to the Addiction™. Now it feels like I am more of a person than I used to be, and my days dont feel as short and unbalanced as they did.

### Takeaway

If you would like to: 
- have your days feel longer
- become better at handling boredom
- be more of a well-adjusted person
- waste less time
- become better at being productive and educating yourself

... I encourage you to kick the Addiction™!
