---
title: "Copyright And Piracy"
date: 2023-12-24
---


{{< picture-big source="/pictures/copyright-and-piracy.jpg" >}}

Software and digital media have a key differece to tangible products that you can buy and use in the real world. They are immaterial goods. 

If you wanted to, you could run a software business out of your own home - with close to zero starting investment. Since software is an immaterial good, you do not need any sort of traditional manufacturing equipment. You do not need a factory to build it and you do not need a warehouse to store it. Neither do you need a shipping service to deliver it to your customers. All you need to get started is a computer, ideally with an internet connection.

It is much the same in regards to digital media. Depending on what exactly you want to do, you might also need a camera and a microphone. The burden of entry in these industries can be a burden of skill rather than one of cost, depending on the specific details.

Digital products are easy to produce in this sence. Likewise, they are easy to copy and distribute. In which ways should we accept the copying and distribution of digital goods? This is a fairly new question for society. And this is where copyright comes in.

What even is piracy? I find it difficuilt to draw the line, so I will stick to clear examples for now. One clear example of piracy would be using a cracked copy of Adobe Photoshop or Premiere Pro. Another example would be watching a movie that is currently playing in the cinema for free on the web. There is probably little debate that those two examples are clear cases of piracy.

You can probably find hardliners on both sides of the matter. One person might say that watching youtube with an adblocker is piracy and should be punished, while another could claim that piracy is always justified. Such a person might argue that all information should be freely accessible, and that copyright is an artificial construct anyways.

Who makes the rules? If powerful industry players are to have influence over them, they surely have reasons to extract as much money from the consumers as possible. Strict DRM in every cell of the web and harsh punishments for all pirates would be their dream, especially if the term "pirate" can be applied as loosely as possible. I'm guessing that the origin of the rules differs from country to country.

What are the problems that come with strict anti-copying policy? Think about social commentary and quotations. In my opinion, it gets especially dangerous when it becomes hard work to stay within fair-use, and when quotations in the form of media can be taken down easily. This of course is not only tried to legislation, but also to the policy of content platforms. Youtube appears to be a very relevant example. I am not a youtuber currently, but people seem to be tip-toeing around content restrictions and copyright strikes a lot.

We could talk about what is piracy and what is not for a long time. If you ask 3 people, you might get 4 different answers. Let's return to the two clear examples of piracy that I brought up earlier. What would be an upright way to deal with them?

My advice would be "Be upright and respect the rules, but don't impose them on others". What do I mean by that? Don't obtain free copies of Photoshop, but let other people obtain free copies of your own products. 
I'm not saying you should provide every single thing you make for free. But please provide at least *something* for free.

If you want to paywall some exclusive content and reserve it for your financial supporters, I don't have anything against that. That is a transparent business model. But if your business model relies heavily on closing every single thing you produce down as tightly as possible and hitting people with lawsuits left, right and center - we might ask the question wether your business model should really exist.

Would it be bad if countries would pass copyright laws that make the the music industry less profitable? Maybe we shouldn't have industries that base their entire existance on playing the copyright-game. But that is not a question for us, but for legislators. 

**Being upright is always the right choice, and it keeps us out of trouble. So what if we can't watch Avengers Endgame or listen to Miley Cyrus for free? Being less involved with consoomerism is healthy for us. Paid, copyrighted products are often not very desirable anways. I'm not interested in pop music and I usually dismiss proprietary software as possible spyware. We have entire free operating systems and can even edit pictures and videos on free software. We can contribute to the adoption of free alternatives. The cost is some discomfort, the reward is saving money, having more user freedom and bringing the free projects forward. By free I mean FOSS: Free Open Source Software.**

### Making Money With Free Products

I really like the idea of "Copyleft", which is deeply intertwined with FOSS. An example of Copyleft would be the following: You license your software in a way that allows others to freely use, copy and distribute it - under one condition. A product that was made by building on yours MUST provide the same freedoms aswell.

While that sounds like throwing monetary opportunities out of the window, there is still room for making money by proving support or hosting the service for others.

The first article on this site in its current iteration has been published in August 2023, but the site has been here before that. I have never had ads on here and I don't plan to introduce them. I have never made money with this site, even though it raised my employability and helped me to learn valuable skills. I could even advertise my own paid IT services here (if you are based in germany, feel free to contact me about them). But none of that changes the fact that I am, until now, operating this site on a 100% loss. And I'm okay with that, if this is the consequence of not imposing a personal-data-driven, anti-user ad model. 

It wouldn't make a difference anyways with the amount of readers I currently have. But I would be much more interested in adding a donation link one day rather than hosting ads.

This has been one of the longer posts. I hope you can get something out of it. Have a great Christmas and remember who it is about!
