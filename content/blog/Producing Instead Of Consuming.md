---
title: "Producing Instead Of Consuming"
date: 2024-09-28
---

The term "producing" is used in a broader manner here. You can produce something in a very direct and obvious way at the workplace. But you can similarily, and even more meaningfully, produce something in other ways. 

As with the spider that I found at my desk, which was producing its string, the means to meaningfully produce are close to you.

A social gathering with your family is an opportunity to produce. You can produce appreciation towards your parents, as well as meaningful interactions and lasting memories. Likewise, you can be a good example and a good advisor to your friend and thereby produce good development and clarity.

You can produce a good habit. You can bear fruit in the right relationship with God.

The last couple of days have been such that I enjoy looking back on! One of the big changes is that I have been abstaining from YouTube. Even though I used PeerTube to fill some of that gap, that still made my content consumption more intentional. 

It feels good to abstain from those videos, even though there was some stuff I did not want to miss. But the weaker the chains of the consumption treadmill are, the stronger we are grounded in reality and the less nervous and unneccessarily political we become. And I have been reading.

Another thing that has been going on is home improvement. We are in the process of re-doing our living room with competent help, which has me looking forward to have a more tidy and friendly place to invite people into. 

I have also equipped my office with some sound absorbing foam, as I did recordings here and there and have been entertaining the thought of a project that could take that up a notch.

When you think of the words in the title of this post, "producing instead of consuming", I can think of two categories of days. The first being those that revolve around yourself and produce nothing, feeding the hunger for superficial stimulation - be that through web videos or other things. And the latter being those that were, in a way, difficuilt - but produced meaningful results.

Ironically, you become emptier the more excessive your consumption becomes. Empty consumption feels good in the moment (at least while your conscience doesn't nag you), but the results feel terrible. With production, it is the other way around.

It feels bad to wake up with the thought of giving up a piece of your weekend to redo your living room, but it feels good to look back on that activity and have a healthy space for a healthy mind. It feels bad in a superficial sence to be investing yourself into someone or something when you would prefer to be elsewhere. But it feels good to know that you have been in the right place at the right time when you see the fruits.

It also feels bad to see the ticking clock in regards to your tax report, but it feels great to recieve a major payback as a consequence of that effort.

Also, I redid my desktop as well as my servers. I distro-hopped. While that was quite intensive, it was a learning experience and produced some automation scripting that might turn out very useful. In addition to that, it should make the process of maintaining my servers less complicated.

What did you produce today? What do you want to produce? Should you really produce what you want to produce?

I hope this post motivated you to do the right thing. Seek objective truth and do objective good.
