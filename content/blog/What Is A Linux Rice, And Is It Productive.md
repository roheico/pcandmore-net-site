---
title: "What Is A Linux Rice, And Is It Productive?"
date: 2024-06-01
---

If you have been below the tip of the iceberg in regards to computing, you might have come across the terms "ricing" or "rice". Even if you don't know these terms, chances are you have at least seen a rice at some point.

Of course, I am not talking about food. Ricing refers to the (especially visual) customization of an operating system. The term is commonly used in the linux world and refers to something that goes beyond merely changing your desktop wallpaper. This is a stereotypical product of the process:

{{< picture-big source="/pictures/qtile-rice.png" >}}

The screenshot shows my desktop. The use of linux with a tiling window manager, the terminal applications and details such as fonts and colors in the status bar make it check the boxes of a typical rice screenshot.

This is not my first rice, but it is probably among the ones I have been on the longest. Such a system can take a long time to produce, depending on the software choices you make and wether you build your configuration from scratch or not. 

### How does ricing affect productivity?

The goal of ricing is often to create an impressive visual experience that can be shared with the internet for updoots. Achieving this to a satisfactory degree can take literal tens of hours, and it is likely that the ricer will re-rice his system sooner or later. This sounds unproductive, doesn't it?

But going through the process of producing a highly customized system can indeed have beneficial effects on your productivity. It gets you familiar with your software stack, as you get to make detailed choices in regards to software and configuration. It allows you to optimize your system for your desired workflow, which would be keyboard-driven window tiling in my case. 

And it adds a personal touch to your system that can make your experience with it more enjoyable. You can build a completely unique setup, afterall.