---
title: "Freedom And Hedonism"
date: 2024-01-21
---

You might have heard some philosophical back-and-forth on the existance of objective truth in the recent years. Even though that feels like something that would be much more present on the internet than in real life, I had this kind of discussion in person a while ago. 

It did not last very long, since the idea that there would be no objective truth can be refuted by simply asking "Is it objectively true that we two are talking to eachother right now?". I am kind of surprised that this discussion happened, but it makes a very relevant point. Namely, that weird and illogical things can seem normal and rational to people when unchallenged.

This is a problem that all of us might be facing, since we have been fed innumerable assumptions over the course of our lifetime. For many of those, we just went with them. And even if we did not in a direct sense, they still influence us and our worldviews.

One extremely popular assumption that people tend to have is the modern, western understanding of freedom. Ask yourself the following question: "What is freedom?". Even though this question might bring up unpleasant memories from literature class, your answer to this question might have a massive impact on all of your major decisions.

The modern, western understanding of freedom is the following: "Freedom means that I can do what I want". And while that is what we often mean when talking about freedom, it does not have very much to do with our lived experience.

Ask yourself the following questions. What do you really want? And, if you were absolutely unrestricted in doing your own will, would you actually do these things?

You might have listed a collection of good goals in response to the first question, while giving a solid "No!" to the second question. Most people would say that they want the following: 

* Maintaining healthy relationships
* Having no addictions to tobacco, alcohol, videogames or  porn
* Being disciplined, educated and productive
* Working out and adopting a healthy lifestyle
* and... so... on...

Those goals are universally agreed upon as being noble. But paradoxically, they are the exact opposite of what we would do if we get our way. If all restrictions on doing our own will would disappear, we would:

* Dive into maximum shallowness by spoiling ourselves with every bit of luxury known to man
* Take advantage of everyone around us, since we would have little percieved incentive to do otherwise
* Become enormously lazy and hedonistic, because our dopamine receptors would be burnt to the ground
* Do away with any bit of discipline, as it would be a burden to our indulgence

Contrast those two lists. The first list presents our goals - the things that we "want to do". The second list presents what we would actually do, if we were "free" in the modern sense.

Of course there are deviations from this. Some people would probably still hit the gym if they had the chance to do anything they want. But in the big picture, our lives would absolutely spiral out of control. And that is true for all of us. We can conclude the following. If we had the freedom to do whatever we want, we would be absolutely enslaved by our lusts and end up ruining our lives.

I don't know where the following proverb came from, but it is very true: "Nobody is more free than the slave of a perfect master".

A perfect master would utilize our labour in the perfect way, while giving us the perfect amount of rest. A perfect master would put us into a situation that is more fulfilling than whatever we could come up with ourselves. A perfect master knows what we need, and he knows the wishes of our heart. And he knows wether those wishes are better to be fulfilled or not.

I can say from my experience that I was not free when my life revolved around videogames and porn, even though I had the ability to indulge in those for hours upon hours. Those things were bad masters in my life that I needed to be liberated from in the first place. Later on, I was able to find absolute freedom in the Lord Jesus Christ, the perfect master.

Deciding to live under the reign of God gave me new motives, new priorities and new foundational principles. I was given the chance to leave my old dirt behind and live a new life with eternal goals. Now, it brings me joy to hang out with my brothers in Christ and help eachother to grow. And it also brings joy to share the message of this freedom with others.

Christ died for us on the cross and took the punishment for all my bad deeds and all of my selfish parasitism. Since he paid for me, I am now free from the things that separated me from walking with God. I believe in his resurrection, and will live forever because of him. And even though I prove that I am unworthy daily, he is loyal until the end.

That is true freedom which I wish everyone to have.

