---
title: "Professionalism VS. Hobbyism In Tech"
date: 2023-09-20
---

A hobbyist who is starting out with something like programming might wonder if there is any chance of him getting to a professional level without working in the industry. As someone who has seen the professional IT space for some time, I think that it is easy to overestimate the skill and knowledge of a paid professional compared to someone who only codes or runs a website in his free time.

If you are a paid programmer, admin or other tech professional, it is very possible that you don't get around to see a whole lot of different technologies. Let us assume that you got into a larger local software company. You landed a job with some sort of specialization, such as being on the Microsoft 365 Helpdesk or being a JavaFX GUI developer.

A somewhat big company can afford to hire separate specialists for very specific roles, such as those I just mentioned. While being specialized in a certain skill is good and marketable by itself, it means that your scope of possible tasks will be rather narrow. The firewall admin will not have to code, and the JavaFX GUI expert will not have to deal with network administration.

What that means for you is that not only your tasks, but also your learning will be restricted to your area of expertise. It is possible to be a "tech specialist" for years and not have done things like using Linux, configuring DNS records for a domain or setting up any self-hosted service.

This is where the hobbyist shines. He has proven himself capable of learning-by-doing, as he did not abandon his pursuits as soon as he faced hardship. He did not know what a subnet mask is, but he found out when he followed a tutorial on setting up a custom VPN. He humbly utters the phrase "I am not an expert", after showing you his well designed non-standard website (which he coded on his Arch Linux rice).

He does not turn when facing his next challenge. Instead he says: "If I could do X, I'll probably figure out Y too". Learning-by-doing is at the core of his skillstack, and he is aware of the value it brings.

If you are a paid professional who has learned to work under the pressure and expectations of real-world clients and companies, be aware of the value of tech hobbyism. Renting a VPS and deploying a website or another self-hosted service is one of the greatest learning experiences you could get.

And if you are a hobbyist, be aware that being a professional does not mean what you might think it does. And be aware that you could probably become one, depending on your milestones and circumstances.