---
title: "Rewriting This Site With Hugo"
date: 2023-08-24
draft: false
---
If you visited this website in the past, you might notice that it underwent a general makeover. 
In the beginning of this project, I used to develop this site by writing pure HTML and CSS. While that worked well enough for the moment, it was quite complicated to manage it. As every post and sub-page was it's own HTML-structured file, a change to that HTML structure would have to be applied all over the site rather than in one central place. Also, it was necessary to add my posts to both the homepage as well as the proper overview list by hand.

I redesigned this website with a static site generator called [Hugo](https://gohugo.io/). This tool allows you to write the general structure of different parts of your website in HTML and CSS, in which you can include content and logic via a simple templating language. Content can be written in Markdown. It is really easy to get started writing content, as you essentially write out plain text with such minimal formatting syntax that it is nearly a What-You-See-Is-What-You-Get experience.

If you already have some experience with basic webdev and are looking for a solution to easily manage your styles and content without working with some sort of bigger framework, definitely take a look at static site generation.
