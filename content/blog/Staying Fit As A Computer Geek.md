---
title: "Staying Fit As A Computer Geek"
date: 2023-09-03
draft: false
---

People would call me a computer geek. And if you are reading my blog, the same might be true for you. With this label come a handful of stereotypes, one of which is being pale-skinned, physically weak and overweight. Those stereotypes do not come out of thin air, as our hobby (or profession) involves sitting in front of a screen for prolongued periods of time. Here are some of my strategies for staying in shape:  


### Taking a walk every day
This one is actually really easy. I try to take a walk every day, with 15 minutes or more being my goal. This is fairly easy to do, as 15 minutes is an amount of time that we can easily hit even on busy days. You can do this in your lunch break, especially if you happen to work in a place where you have a mandatory hour to take off for the break. Perhaps you could even move a meeting outside and turn it into a walk, if it only involves 1 or 2 other people.  


### Working out at home
I used to go to the gym, probably something like 3 years ago. Typically, people do not go to the gym for just half an hour. If you have a serious, more intense workout routine and account for factors like driving to the gym and changing clothes, you'd probably be there for significantly longer. Nowdays I am not willing to put in the hours.  

I moved to working out at home instead. While I can recommend this, make sure you know what you are doing. You want to know how to work out without injuring yourself. You can set up a minimal home gym with just a pullup bar, some dumbbells and a mat for floor-level exercises. You can already do a lot with such a minimal set of eqipment (or even less). You won't need to pay for a gym membership and since you don't need to drive anywhere, you are saving time aswell! 

I dont do very much currently, but I try to get a set of pushups or pullups in every now and then. While I would like to be doing more, it still seems to make quite the difference in comparison to doing nothing. Also, working out at home helps you to focus on staying fit rather than being dragged into hobbyist bodybuilding.  


### Stretching
Stretching is another one of those things that are free. As someone who has been un-flexible since childhood, I am happy about the results! While it can take weeks or months to see a big difference, stretching is a habit that is fairly easy to maintain. I currently do a routine that is easy to do daily, spanning only a handful of exercises. One of which I do to prevent carpal tunnel issues. Find some quality, non-yoga information on stretching. Yoga is connected to eastern spirituality (or deeply rooted in it), therefore I would stay away from that.


### That's it!
I hope this post gave you some ideas on how to stay in shape (or improve it) even if you don't have a lot of free time. What do you do to stay fit?