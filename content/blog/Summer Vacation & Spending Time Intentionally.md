---
title: "Vacation In Austria & Spending Time Intentionally"
date: 2024-07-27
---

The last two weeks have been quite eventful. I spent a week in Austria, which neighbours my home counry of Germany. It was a group trip that involved a bunch of hiking in the mountainous area, as well as swimming in a lake and nourishing ourselves with the stories of people in the Bible. 

Since it was a group trip, it was very easy to leave the room and get busy with social interaction - or to go and intentionally spend time alone. One of the best things was getting away from the phone for almost all of the time there, which was refreshing.

After that trip, I am having roughly another week at home (which is now approaching its end). I started exploring a method to make better use of my time here: Using the countdown alarm on my phone. It helped me to put an hour or more into learning greek for 3 or 4 days in a row so far. Another good use for that is filling the time with intentional, non-digital activities. Setting a timer to do things has worked really well for me so far, perhaps because of the tangible challenge (or even game-ification) it introduces. It is amazing how life changes when the distractions go silent.

Another thing is that my website has been down while I was on vacation. This happened because my subscription at a hosting provider ended. I migrated the site to the provider of my preference, so now it is back up again.
