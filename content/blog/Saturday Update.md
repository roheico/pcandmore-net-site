---
title: "Saturday Update"
date: 2023-09-16
---

Ive come out of some time-pressure recently and took some time to work on this site. Yesterday I posted a guide on [writing documents in Neovim](../../guides/writing-documents-in-neovim), after finding a nice way to replace LibreOffice for real-world writing tasks. Feel free to check it out if you are interested in that.

Also, this site has an RSS feed now. More exactly, one that shows you the full text of my posts. I have set up an RSS reader to stay up to date on cyber security news. Besides that, RSS is also a pleasant way to keep up with one's favourite bloggers.

After spending a lot of time on the internet today, I'm happy to have been outside in the sunlight. I might try to do morning walks on saturdays to feel more fresh from early on. It is not pleasant to be stuck at the screen all day, and countering that from the morning on must be healthy.