---
title: "Replacing Google Maps With OpenStreetMap"
date: 2023-12-16
---

Most people are going to see online privacy as something desirable, at least to some extent. But oftentimes this desire is overtaken by that for ease and comfort. Additionally, free and more private solutions tend to seem complicated at first. Either by forcing the user to adapt to a new way of doing things, or by lacking convenient features.

I care about my privacy, and have been willing to go through a lot of discomfort in order to turn my system(s) into something that I am happy with - both from a privacy and functionality standpoint. There are some changes that are easier to make than others. For example, it is easy to adapt to a new mail client. Switching from a proprietary mail client like Outlook to, let's say, Thunderbird is not a big change. You install the program and log into your mail accounts another time, that's basically it.

Then there are things that bring more discomfort, such as selfhosting. While it does not have to be very hard down the road, we can't pretend that it wouldn't be a serious technical skill to learn. Even though it is easy to follow a (current!) video tutorial, a self-hoster should want to know what he is doing.

One thing that can be hard to leave behind is Google Maps. I can't say a lot about Apple Maps since I never used it, but what is true for Google Maps is probably also true for the other big, proprietary navigation solutions. Even though there are more privacy-friendly alternatives around, the idea that you lose Google's accurate traffic data makes using one of them sound nearly as troublesome as learning to self-host! At least if you drive a lot and therefore draw value from Google's reporting on traffic jams.

Around a month ago, I decided to give an alternative navigation solution a try. I decided to go with Organic Maps from the F-Droid store. This app uses OpenStreetMap, which is a dataset that you can contribute to. In fact, I even know someone who told me that he did some mapping for Africa. 

While relying on volunteer-provided map data sounds like it is prone to outdated routes, I would like to point out that Google Maps definitely isn't perfect. Google Maps works quite well here in Germany, but I had a somewhat traumatic experience with it in Greece. And if I remember correctly, Google Maps was not always up to date on construction-based route changes here either. So far I can't tell which option is going to be more up-to-date and correct. So there is no winner in this point for me.

In regards to the user Interface, I find them pretty similar. Google Maps may have some more convenience to it, but in the end they both are navigation systems that behave much as expected.

A big change that I did experience is that I now rely much less on navigating with an app. Since I don't expect to be warned of traffic jams, there is much less motivation to turn on the navigation for every longer drive. Also, I can't notice a serious increase in time spent in traffic jams. It might even have stayed the same or gone down since there are no overrated suggestions for alternative routes.

Would I make the same technical choice again? Definitely. I do not want to supply a company with my real-time motion data. And being able to go FOSS on this one makes me happy, as it generally does with other applications. The loss in convenience really was negligable, and contributing to the popularity of an open map dataset is one of the most effective ways to increase the quality of it. 

Did you make similar experiences with going for FOSS alternatives to popular software? Feel free to drop me a mail and tell me about it.
