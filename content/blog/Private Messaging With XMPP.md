---
title: "Private Messaging With XMPP"
date: 2024-01-20
---

Privacy & software freedom have been part of this blog's main theme. I am not an all-out privacy guy who goes above and beyond to cut out any last sort of fingerprinting out of his life, but I sure do enjoy to have control over my digital life.

The latest project in line was setting up a private chatting solution. Hosting an XMPP server was the most appealing solution to me. It does have a better reputation for privacy than Matrix as far as I have seen, aswell as being light on server resources.

Those two solutions are very similar. Both XMPP and Matrix are protocols/standards that live within a multitude of server- and client applications. Thereforce, you can pick from a variety of different chat clients. Different clients will have support for different protocol extensions. It is an evolving ecosystem. You can find more info on [the standard's site](https://xmpp.org/). This kind of system comes with benefits, such as: Being able to pick the best client rather than beeing peer-pressured, and being able to ditch a piece of software and replace it if one of the projects goes sour.

Either XMPP itself or a variation of it is used in WhatsApp and Zoom among other popular solutions, as seen [here](https://xmpp.org/uses/instant-messaging/). If it is reliable enough for them, it is reliable enough for me. And DIY'ing it allows us to do cross-server chatting. The idea behind such federation is a lot (or exactly) like email, where one can use a gmail address to talk to a yahoo address.

I decided to host my own server rather than just jumping into the network with a random public server. Using my own domain for this and advertising my account does negate a lot of the privacy that I would gain otherwise (though, clients support using multiple accounts). But I like having a "branded" XMPP address as an official option for contacting me, as an alternative to email. Afterall, it is not only a matter of privacy, but also of principle. You can find my current XMPP address on the CONTACT page, and you are welcome to drop a message there!

Setting up the server was fairly straightforward in itself on NixOS, as there are official packages and options for the **ejabberd** and **Prosody** server implementations. It did take some troubleshooting since I misunderstood a part of the configuration and had issues with DNS, but that is not the project's fault.

Instant messaging works fine for me, so does sending a voice message. My first attempts at calling a friend via voice chat failed, but I am not sure wether that was due to him using a different client or due to my broken config at that time. The feeling is very similar to that of a conventional instant messenger. In the end, the athmostphere is going to be set by your XMPP client.

XMPP seems like a solid option for talking to friends and family outside of WhatsApp and Telegram, with proper end-to-end encryption. It also offers uncompressed media sharing, which requires a workaround on WhatsApp (if it is even still possible). There is a drawback though. Many clients that are featured on the XMPP website seem to be unavailable on Google Play. It doesn't matter to me personally since I prefer using a client from the F-Droid store anyways, but keep in mind that you might need to help your friends with that. Maybe hosting an instance is a good option for company-internal communication too, especially if you are going to be using uniform work devices with a standard choice of clients.
