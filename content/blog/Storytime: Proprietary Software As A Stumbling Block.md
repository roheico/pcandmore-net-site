---
title: "Storytime: Proprietary Software As A Stumbling Block"
date: 2023-09-23
---

{{< picture-big source="/pictures/Bike Fall Proprietary Software 2.png" >}}

I have been a Windows user for most of my life. From our big old Windows XP machine up to Windows 10. I was as used to the Windows way of computing as one could be. When I think back now, it feels like I escaped a sinking ship at the proper time. Perhaps the landscape of proprietary standard software became worse since I made the jump, or it has been like it is today and I just haven't noticed.

Whichever of those it be, I am really not impressed by what the typical standard software product offers. I mean free and paid, non-open-source software like Windows, MS Office, Google Chrome, MS Edge and so on.

Nowadays I am under the impression that Free Software is not only a valid alternative to those tools, but literally better and more usable. I am going to tell two recent stories to make my point. While I want to keep my blog wholesome and uplifting, I see purpose in the following critique of standard software. Nowadays, pointing out the "oopsies" or the anti-customer practices of tech megacorps is some of the most effecive Free Software advocacy one can do.

### Example 1: Slideshows from a fresh Windows 11 installation

I wanted to deploy a slideshow on a handful of big displays that are attached to Windows machines. So far, so easy. Right? The slideshow in question was not a typical collection of photos, but instead it was made up of multiple informational slides or documents. Therefore, I needed a longer display time per slide. We settled on 5 minutes per slide.

Windows has a built-in photo viewer. It indeed has a slideshow feature, which seems like a great tool for this task at first. That is, until you find out that it does not allow you to change the duration of your slides.

For some reason, Microsoft allows you to use the old legacy version of the photo viewer instead. So I took a look at it, as it could have been an easy solution. Nope! No slide duration settings either. In the end, I installed VLC. This free solution actually allowed me to set the duration for my slides and deliver the show. 

I would expect the Windows photo viewer to be able to handle this absolutely tiny task. Especially since it is automatically provided with a paid operating system that is developed by a rich giga-corporation. I understand that they probably have their own frameworks or patterns that might make the addition of such small features more difficuilt than it seems, but can it really be so much more than allowing the user to set the value for a single variable? A free and transparent open source solution (developed by people nobody knows about) did a significantly better job than the Microsoft solution here.

### Example 2: Installing MS365 on Windows

The software repository system is one of the things I appreciate the most about Linux. You tell the computer that you want to install the web browser of your choice. The computer pulls the software package from your distribution's repository. It either prompts you to confirm what is about to happen or just finishes the job without any questions, depending on the command and distro you are using. It makes installing software a more automatic, easy and safe task.

With Windows, there is the Microsoft Store nowadays. I do not like the "Store" part, because it sounds like you are going to spend money. I want to use free community-oriented solutions instead. But it fits with the mobile STOREs, so I guess they also picked this name to make the phone user feel more at home. I am not an active Windows user anymore, but the store feels pretty limited to me. And appearently Microsoft's system for checking apps that get published on the store [has not been entirely airtight](https://www.forbes.com/sites/leemathews/2022/02/24/malware-sneaks-in-to-windows-store--disguised-as-popular-games/). That is not surprising, since the software repositories of the Linux world could be compromised aswell, but remember that the user pays for the MS Store as a part of his Windows setup.

Besides the store (and "winget", which sources packages [from the MS Store and another repository](https://learn.microsoft.com/en-us/windows/package-manager/winget/source)), there is still the historic way of installing software on Windows. Namely, hunting for installers while dodging fakes, mainly on the ***Open Internet™*** as provided by Google or Bing.

Here comes the actual story for this one: We wanted to install the Office365 Apps on relatively fresh Windows machines. So far, so simple. The installation got stuck at 0 or 3% on two or more machines. In one case, plugging the machine into the network via a cable seems to have been the solution. Not so in the other case!

In the other case, there was no clear reason for the defective installation process. No amount of restarting the installer or deleting registry content solved the issue. After jumping through some hoops, I managed to get a working installation via a separate deployment tool provided by MS. Interestingly, this major installation hickup happened with a Microsoft Product on a Microsoft OS. One could have expected a more painless integration between the two, or at least an easy way to get an offline installer.

What about a Free Software alternative? I had an issue related to visual themes with LibreOffice. It was probably my own fault, as I don't use a pre-configured desktop environment. But at least I can reliably install it and use it for free!

### Takeaway

There is more that could be mentioned. For example, a service did not allow my wife to share a link without being logged in. It does not really surprise me how many people don't get beyond a rather limited degree of computer literacy, when this is the experience they get when stepping outside their web browser.

Oh, then there is also the privacy issue with closed source software. :)