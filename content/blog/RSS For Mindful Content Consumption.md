---
title: "RSS For Mindful Content Consumption"
date: 2023-09-30
---
If you are a regular reader of personal blogs, chances are you have stumbled upon RSS feeds. While this technology is not new to me, I only recently commited to it and built a more serious feed library.

For those who have never used it, RSS (including similar, compatible technologies like atom) is a way to build a social-media-like feed without social media. You can download an RSS feed reader and add the feeds of your favourite websites (or even youtube channels) to it. Then, your feed reader will allow you to see the latest posts made on the original site. This way, you can have a general subscribtion feed that covers as many sites and services as you want.

Some feeds are configured to display full-length content, allowing you to read entire blog posts inside your reader - including pictures! Other feeds allow you to see the latest headlines, so you can open a post inside your browser if you want to read the full content.

I grouped some security related feeds into a larger one that helps me to stay up to date on cyber security news via a single page in my reader. This is an actual, practical use as it saves me the time to browse multiple pages separately.

For normal content consumption, using RSS removes the For-You-Pages, the content recommendations and even the ads. This makes it easier to consume more mindfully, since you bypass the algorithms. Feed readers come in different forms. The one I use on my desktop is a terminal based reader called newsboat. Below you can see what it looks like. There are graphical readers too, but I like being able to pull up my reader on the terminal.

{{< picture-big source="/pictures/newsboat-screenshot1.png" >}}
{{< picture-big source="/pictures/newsboat-screenshot2.png" >}}
