---
title: "Update: My Email Is Back After Downtime"
date: 2024-01-20
---

This is a short update for those whom it might be relevant for. My Email server is back online. If you sent me an Email that I did not reply to, you can assume that it did not reach me. In this case, just send it again (or, use my XMPP address instead).
