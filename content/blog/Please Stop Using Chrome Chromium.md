---
title: "Please Stop Using Chrome/Chromium"
date: 2024-01-28
---

Looking at recent numbers, the market share of Google's Chrome browser is at 65.29% on the desktop, [according to statcounter](https://gs.statcounter.com/browser-market-share/desktop/worldwide). The numbers vary depending on the statistics provider, with [this one](https://www.similarweb.com/browsers/worldwide/desktop/) giving a 63.33% figure. I use the numbers for the desktop in both cases, since it is the most relevant platform in terms of "the open internet".

This figure itself is not shockingly high, considering that Chrome's popularity is common knowledge. But it is easy to forget that other browsers, too, use Google's Chromium base. This means that we can add the numbers of all other browsers which are built ontop of Chromium. Perhaps most notably, the list of such browsers includes Edge, Opera, Vivaldi and even Brave!

Adding the numbers and dropping Safari from the equation, as it is part of the Apple ecosystem, Google has an absolutely massive grip on the web browsing market! Good for them, one might think. Afterall, we still have firefox and other, more niche projects. But what is the problem with the nearly universal adoption of Google's browser technology?

## The Problem - Part 1

Market share equals control. If Google has control over 90% of the browser market, they are free to push their standards and policies to nearly the entire browser userbase. Most users, being less interested and informed on this topic, are bound to go along with whatever trends Google aims to set. It is bad enough that more than half of all desktop browser users use an intransparent, closed-source, proprietary browser. Due to its proprietary nature, this browser handles the most personal pieces of user data in an unsupervised, potentially malicious way.

This data includes login credentials, some of which are tied to their owners' livelihoods and bank accounts! Besides the OS itself, the web browser is the most crucial battleground for digital privacy. Even if you, for some nebulous reason, don't mind being datamined by a giga-corporation - be aware that there is more trouble to it than meets the eye. Google, Microsoft and the other dominant tech companies are not immune to data breaches. And if they store all of your personal data, it too may end up being compromised due to such a breach. 

The same is true for other proprietary browsers, which is why I strongly recommend staying away from all of them. I myself refuse to use Chrome and Chromium-based browsers, especially on the desktop, outside a narrow and mindful set of conditions. Those conditions include rare reasons of compatibility, or being on something like a Windows VM or a very specific software setup. To back my point about the possibility of tech companies losing your data to malicious third parties (though data brokers are already malicious enough), I point you towards the following story. Recently, [a case made the news](https://www.wired.com/story/microsoft-hpe-midnight-blizzard-email-breaches/) in which Microsoft suffered of a breach of email accounts, including members of their senior leadership team and their cybersecurity (ironic, I know) and legal departments! If they could not protect this kind of data, who guarantees that they can protect yours? 

In my opinion, we would be better off by using technologies that don't put us at risk of being affected by nation-state level cyber espionage or data breaches outside our own control.

## The Problem - Part 2

Assuming that everyone would use a hardened installation of [Ungoogled Chromium](https://github.com/ungoogled-software/ungoogled-chromium) rather than Chrome to dodge the privacy issues, another problem would remain. Namely, that Google would keep the power of pushing new web standards. A recent example would be [Googles web DRM proposal](https://www.theregister.com/2023/11/02/google_abandons_web_environment_integrity/). This proposal fell onto unwilling ears and failed. Had it succeeded, it would have given website owners a tool to block any visitor whose hardware and software do not fit certain criteria. Imagine how such a technology could be used to bend the market share further in favour of a monopoly, if popular platforms like Spotify were to implement it.

In addition to a uniformity of web standards, a browser monopoly would lead to a uniformity in web exploits (including zero days), which is a nightmare for even (or, especially) an advanced user threat model.

## What Can We Do?

We users have the option to drive up the market share of Non-Chromium browsers, such as Firefox and it's derivatives. As long as we can keep other browsers around, the market can adjust in favour of users if the Chromium squad pushes for new restrictions. The further we are away from a 99% to 1% situation, the bigger the odds of improvement. Though I am optimistic that an alternative would emerge, even in the event that Firefox would cease to exist.

When choosing an alternative browser, make sure to read the small print to see wether there is any Chromium or Blink (Chromium's browser engine) in there.
