---
title: "Things I Would Like To Learn"
date: 2023-10-15
---

One of the great things about our current age is that you can learn anything for free, more or less. After funneling unhealthy loads of my freetime into gaming as a kid, I came to see spending my time to build and learn as a great alternative - especially in combination with a more balanced and healthy life. Here are some of the things I would like to learn, or am in the process of learning. 
 
 
## Biblical Greek
Biblical greek and hebrew have been a long-time interest for me, starting at the age of 16 or 17. Back then I didn't have actual books on the languages or an actual, professional course. Now I have been studying greek at home for over a year. I learned the basics with a textbook and am trying to keep up my knowledge and increase it slowly, and am really happy that I started back then rather than waiting for another couple of years. My biggest regret in regards to this is that I didn't start earlier. I want to develop better studying habits, get a stronger foundation in regards to grammar and read through the entire new testament eventually.

## Foraging
I do love being in nature, even though I don't go into the forest or such things as much as I would like to. One of my favourite things this year was learning about an edible berry that is quite common here. It is so common that knowing it could make a difference in a survival situation, and I have been going outside and eating it from time to time. It feels really cool to interact with nature like that. I would love to find some more edible berries or other fruits, especially such that are less known and obvious than things like wild mini-strawberries. 

Funnily, while this post was still in the making, my parents introduced me to another edible berry that I had no clue about. Apearently the fruits stick to the plant even into the cold season with this one. Great stuff!

Also, I would love to know 2-3ish edible, easy-to-identify mushrooms. I want to play it very safe though if I really get into the topic, since I recently heard about someone who died due to eating the wrong mushroom. And it would be great to know about an edible root that one can dig up in the forest, or other plant parts. 

## Fossil Hunting
We have a river nearby, where I have found 4 fossils already. Nothing especially big or exotic though. I found all of them in the stones on the river bank, without the use of tools. I would love to find something bigger, such as an entire fossilized fish. But that would probably require some more knowledge about which stones to look out for and how to crack them without damaging a potential fossil. Anyways, maybe I'll get around to it one day.

## Takeaway
Running this blog and other selfhosted services is also a learning experience for me. And I had a great experience with doing that instead of wasting the time with less useful activities. In many things, we can get very far by buying and reading a book, experimenting or following internet tutorials. What do you want to learn?