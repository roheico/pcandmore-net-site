---
title: "September 2023, Still Not Using ChatGPT"
date: 2023-09-04
---
Since the release of ChatGPT in November 2022, AI has been one of the big things in the public eye. Even now that the hype has worn off, the future of such large AI and capable AI models is one of the major questions of this time. Will the robots take our jobs for real? Will AI put an end to human creativity?

While it is for sure impressive that we are able to generate realistic pictures nowadays - or to prompt the robot of our choice for answers on tough questions, my interest in those technologies is still very limited. There are things that keep me from relying on AI for coding, doing research or forming opinions. Especially when we are talking about models that are in the hands of corporations. 

Even if AI does a better job than search engines at providing us information, there is something that generally makes using those AI tools much less appealing to me than experiencing the products of actual humans. There is something beautiful in being less dependant on machines and doing our own thinking. Imagine the following choice: You are placed before four pairs of media. Two videos, two blog posts, two news articles and two technical papers. Wouldn't you prefer the organic content made by an actual human in all of those cases? 

Human creativity is an actual miracle. And the unique beauty of actual human life is something that can never be replaced with automation. Any major attempt to replace it would leave us more empty. The difference is comparable to that between going out and seeing nature or being surrounded by gray buildings with polluted streets all around and no chance of escaping. While we can live under lots of circumstances and get used to almost everything, there is much non-reproducible beauty to "the more natural thing".

So far, I lived through the entire hype-episode of popular-level AI without ever using ChatGPT. And it feels like I did not miss out on anything. Living in a state of informational independence keeps me able to read technical documentation, have my own ideas and I don't have to fear that my robot would mislead me with Information that it just hallucinated up. Or with a pre-programmed instinct of sticking to official, pre-approved trails of thought.

Maybe automating ourselves out of reality more and more will help us to appreciate what is left of it. Actually a lot is left. we just have to search for it. And I wish you great success in doing so!