---
title: "Bits And Pieces (May 2024)"
date: 2024-05-20
---

This is a mixed update post, in which I collected some bits and pieces from the recent weeks. Hoping to be back with more technical content soon enough. 

### My friend got married
Recently, a friend of mine got married! I am really happy about that and have never been as involved in someone elses wedding as this time, which is an appreciated honour. One exciting moment was being stuck on the parking lot when trying to head to the church ceremony after taking photos (I drove the wedding car). The ticket reader refused to take our parking ticket. In the end, we received help and did not lose much time. Wether that was a user error or a technical issue, it serves as a reminder that technology does not always make our life easier or better.

### Blogging and other tech stuff
Sadly, I havent been as able or willing to post here as I would have wished. I had this rough goal of publishing at least one post per month. Even though it is not much, it can easily be overshadowed by other things when your mind is elsewhere. I try to post things that have some length and relevance to them, rather than sticking to a quota just for the sake of it. My hobbyistic IT activities (which provide content ideas) have been on a weak flame in general - which doesn't sound too bad now that I typed that out.

### Studying Greek
The recent weeks have been quite successful in regards to studying biblical Greek. My confidence has gone up and I am consistently managing to read bigger chunks of text. Even though it took a long time to get to this point and there has been plenty of stagnation and/or knowledge loss on the way, I'm seeing the fruits of sticking to the effort. What seems to have helped is shifting the focus from doing vocabulary drills to actual reading, which is what I am studying for after all.