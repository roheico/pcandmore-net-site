---
title: "A Linux Nvidia Laptop Adventure + Followup"
date: 2023-10-27
---
Let me start off by illustrating the initial situation. I had a laptop with high 2019 specs laying around. This device is somewhat dear to me. I bought it back when I was still on Windows, and it is also the device I made my first big Linux steps on. Steps like installing Arch Linux, which I probably did 14h times or more until I was satisfied with the result. This is the device I even purchased a digital Windows license for. And that from the Microsoft Store.

Recently I decided to pick it up and began turning it into my daily driver once again. I don't plan to use it for gaming, but the high specs are sure useful for VMs. It also has a mobile Nvidia RTX 2070 Max-Q, so I appreciate the hardware support for video computing and the tensor cores - both of which I might end up having some use for.

Every more involved Linux user knows that Nvidia is notorious for compatibility issues with Linux. I have dealt with Nvidia on this device in the past and thought I had figured it out, at least to a sufficient degree. This time I had a specific setup in mind. I wanted to use my Intel GPU to power my normal usage and have my dedicated GPU turned off unless I explicitly want to offload tasks onto it.

By this time I started using the Hprland compositor, which does not handle Nvidia very well. I ran into display bugs and overall had a frustrating time troubleshooting my Nvidia issues. There was no way to make it work if Hyprland had any contact with my Nvidia card with mainstream Nvidia drivers. And that would always be the case since appearently all of my external outputs are hard-wired to the Nvidia card. No Nvidia card, no external monitors. Not even Hyprland Nvidia patches resolved the issue.

So I stood before a choice. Option one would be switching to another display stack, perhaps even back to Xorg. After having made the transition to Wayland and ricing my Hyprland setup to what I consider near perfection, I chose to go with option two - giving the open source Nouveau driver a shot.

The result: It works! I can finally use this machine more or less as wanted. There might still be some bugs to iron out with things like power management. But most of the work should be done now. I even did undervolting and other optimizations to improve the experience. I am quite happy with this setup and have put it to the test successfully so far.

There is even a way to offload applications onto my dedicated GPU with this configuration. When testing that, I experienced some white pixels when playing video. But that's better than nothing and seems to be situational. I wonder if the offloading feature really makes a difference though, since the card's performance might be just as low if not lower on Nouveau than that of the Intel GPU. 

One side-effect of this entire adventure is that I have a system without any unfree-licensed Nix packages now, since I don't need to make an exception for my GPU related software anymore. I might just keep it that way and use a VM if I happen to need a non-free package again.


## Followup

I changed my setup shortly after posting this article, since the setup was just too glitchy. Maybe things will look better in a year. For now, bspwm it is!