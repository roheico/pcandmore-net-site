---
title: "Visiting A Hackerspace For The First Time"
date: 2023-11-17
---
The internet has plenty of impressive geeks. But did you ever wonder where all of those people are in real life? I am talking about the ones who do their entire computing on Gentoo or on an Arch Linux rice. Or the ones who provide niche technical answers on reddit threads that OP would never have discovered on his own.

I was doing some research on Linux User Groups, as I am interested in socializing with locals through something like that. While browsing around, I found a local hackerspace. The term "hackerspace" does not refer to a den of cyber criminals. The term "hacker" in this sence refers to a creative computer tinkerer, so a hackerspace is a community-oriented shared workspace where hackers hang out and work on projects (either together or independently of eachother).

Previously, I had never been at such a place and did not have a very clear idea of what that would look like. As I had time on my hands, I decided to go there and check it out. In the best-case scenario, it would be a group of techies with shared interests but diverse specializations, who would also be ready to socialize.

And as far as I can tell from this first visit, that is exactly what it was like! It was a shared workshop and workspace for doing things from 3D printing to electrical work and ham-radio-ish stuff. The people were friendly and I met another guy who was there for the first time. A bunch of them must have had beards. Overall, it was quite pleasant. And not a single Windows machine or MacBook in sight.

Having this kind of shared workshop is a great opportunity for trying things like 3D printing, or for projects where one person would program a microcontroller and another would manifacture a fitting case.

This experience definitely had something that other social occasions with other people who are invested in IT did not have. In comparison, my time at the hackerspace lacked the corporate character that more "professional" interactions tend to have. That is definitely appreciated. I am looking forward to going there again at some point in the coming weeks.

The idea of this visit was to see what it's like to visit the hackerspace and make some first contact. Since that was the rough plan, I made the grave mistake of not taking my laptop with me, and also did not stay very long. In retrospect, it would have been really nice to spend more time among those people and their characteristic ThinkPads.
