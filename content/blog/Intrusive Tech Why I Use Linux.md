---
title: "Intrusive Tech / Why I Use Linux"
date: 2023-11-11
---

What makes people change their behaviour? In this case, from doing things "the normal way" to something as counterintuitive as abandoning most of the software you have been using for years. Many people never really experienced another way of using a computer than using Windows or Mac and running things like Microsoft Office. 

Since the greater part of society just subscribed to using standard software, people accept all of the anti-customer and surveillance elements with a "that's just how things are" attitude. We know the saying "Google and Facebook have my data anyways".

The first computer I have memories of was our family's big old Windows XP box in the 2000s. In those years, we were using black and white phones without Mobile Apps in the modern sence. There was a reality that was more separate from the online world, and technology was not as invasive as it is today.

The rise of the smartphone happened and online communications grew more and more advanced, giving us things like end-to-end encrypted text messaging that was free of charge as long as you would be connected to the internet. Cameras also got better and the social media platforms grew.

In the shadow of the intriguing possiblities of company-owned gigaplatforms, there were those who raised privacy concerns and warned of surveillance. Those warnings, be it rumors or true information, reached the broad mass of society. But instead of ditching those technologies, people just silently agreed to being okay with the privacy implications. It became the standard to allow tech companies to use us as playballs. After all, their data collection is what makes targeted advertisement possible. And advertisement is what kept many of the online services free.

The trend of not caring about our privacy and our control over technology progressed. When installing Windows, it's normal to have to say "no" to telemetry multiple times. And even after all of that, one still gets the impression that the companies act like they own our PCs. Invasive telemetry is just as normal as being forced to create an online account. You know, to use a device you physically bought. That's just how things are afterall. We live in 2023.

The straw that broke the camel's back for me was security. Privacy-less "normie computing" sounds bad enough. But since so many millions use computers this way, this is where the malware makers are at. Like vultures, circling over the heads of gamers and grandmas alike. They are waiting for them to run malicious executables that the user won't spot because Windows hides file name extensions as if those are too confusing to understand for the average computer user.

Overall, I find that "normie computing" not only is laden with privacy and security issues. It also feels like many things that people do are learned ignorance. I want to learn and make technical decisions about my system myself, rather than being sold a closed and uniform standard experience. But your freedom to do that on the average smartphone or Windows PC is very limited.

So I decided to install Linux. Since I was a more technical user, I decided to try Arch. After perhaps 14 attempts at "getting it right" with the command line installation, I had my system up and running. Finally I could choose wether I wanted to use the Budgie desktop environment or even a tiling window manager. And I had the freedom to keep things local-only rather than making online accounts. 

Before settling for NixOS, I also spent some time on Artix and on Linux Mint, the latter of which feels a lot like a better version of Windows. I have seen so many useful and pleasant pieces of software that I would litterally miss Linux software if I were to switch back to Windows. And that despite the common notion that people would miss their Windows apps!

I even put some less technical people on Manjaro and Mint, and they are doing well! Now they have the same luxury of being hardened against corporate entitlement while keeping a fair ease of use, especially since most things are done in the browser nowadays. Mint allows you to have the polished out-of-the-box experience that people want, while you can still choose to go the extra mile to have a very unique and custom setup on another distribution.

Another advantage of using Linux was that it made me more familiar with servers. I manage multiple servers currently. And desktop Linux is what sharpens my axe, since the skills I learn there translate straight to the server space. 

One of the greatest arguments for switching to Linux is the Free Open Source Software (FOSS) philosophy. People seem to be very surprised when they hear that there are mature and free anti-surveillance alternatives to more or less everything they use. Programs being Open Source or ideally FOSS became a pretty strict criterium for my software choices. 

This turns even more relevant as corporate tech is becoming increasingly intrusive, as recently seen with the new MS Outlook that [sends your email login credentials to MS](https://www.heise.de/news/Microsoft-lays-hands-on-login-data-Beware-of-the-new-Outlook-9358925.html)! Companies are demonstrating time after time that they can not be trusted with our data, be it through privacy violations or by making [embarassing security oopsies](https://www.schneier.com/blog/archives/2023/10/cisco-cant-stop-using-hard-coded-passwords.html).
