---
title: "About"
---

My name is Roman and I live in Germany. I run this site as one of my hobbies. Another hobby of mine is cycling / gravelbiking.

I started using linux some time in the recent years, coming from a windows background. As someone who likes to do deep dives on things I get interested in, that led me to going down the privacy and software freedom pipeline further and getting into selfhosting my own stuff to a serious extent, which in turn produces topics to blog about.

Other things I am very interested in are my christian faith and the languages of the Bible. I have been studying biblical greek in my spare time and would love to get deep into hebrew as well over the coming years. I love fruitful personal improvement, which I see as a fruit of biblical spiritual development. The teachings of the Bible inspire me to adopt thankfulness and anti-consumeristic thinking, and to target goals of eternal value that are far greater than chasing riches and pleasure.
