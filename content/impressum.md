---
title: "Legal Information"
---
_The following information is provided due to the legal situation in Germany. I do not live at the address provided below.
Please do not try to contact me using this information (except for legal matters). Use the Information on the contact page instead._

### Legal Note / Impressum 

_Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV  
Responsible for the content on this website_  
  
Roman Heiser  
c/o Block Services  
Stuttgarter Str. 106  
70736 Fellbach  
Germany

### External Content / Externe Inhalte

This website loads media from remote URLs and links to external sites. I am not liable, to the extent allowed by law, for any damages or other consequences that occur due to external content. The same goes for possible false information in external content. Websites can change, get compromised or change their owner. Always click links at your own risk.

### Data Protection Disclaimer / Datenschutzerklärung

By using this website, technical data such as your IP address is exposed to my underlying infrastructure, which is hosted by an external provider. Logs that are created in the process may be used to resolve technical issues. I classify such collected information as technical, rather than personal.  

As there is no creation of user accounts or similar features, no personal data is collected, stored or processed.