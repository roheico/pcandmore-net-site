---
title: "Spotlight"
---

{{< spotlight-entry pic="/pictures/linux-mint.png" color="#82f149" url="https://www.linuxmint.com/" title="Linux Mint" desc="Thinking about Linux but unsure about where to start? This is my recommendation!" >}}

{{< spotlight-entry pic="/pictures/screenshot-20241026-0942.png" color="#853070" url="https://rubjo.github.io/victor-mono" title="Victor Mono" desc="My favourite terminal font that gives me cursive italic text" >}}

{{< spotlight-entry pic="/pictures/codeberg-logo.svg" color="#1193f3" url="https://codeberg.org" title="Codeberg" desc="My favourite alternative to Microsoft Github" >}}